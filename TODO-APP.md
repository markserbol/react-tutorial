# Create a Simple React Todo App

### Setup

Gamiton ta lang ang [react-create-app](https://github.com/facebook/create-react-app) nga starter kit. Kung wala mo pa na setup download lang ang [npm](https://nodejs.org/en/) LTS version, tapos:
* open cmd kag `npx create-react-app todo-app`
* change directory sa project folder - `cd todo-app`
* kag `npm start` para mag open sa browser `localhost:3000` 

### Goal

Create a React single pag app that:
* adds a task from an `input`
* shows list of tasks with:
  * two categories -- undone and done;
  * a `checkbox` that set a task as **done** when *checked* and set it back to **undone** when *unchecked*;
  * a button to **remove/delete** the task
  * and an `input` to search a task


### Approach

Una ubrahon ko analyze ko anay data so kay **list of task** dapat `array` na sya. Dayun per each task kinanlan ko save ang details sang task as `string` text, tapos ang status **done** or **undone** pwede ko save as `boolean` kung done ang value `true` kag kung undone value ya `false`. So gwa sang data list ko amo ni:
```javascript
[
  {
    id: 1,
    details: 'sample task 1',
    done: true,
  },
  {
    id: 2,
    details: 'another task',
    done: false,
  },
  // and so on...
];
```

### Start code

Sa project folder makadto ko sa `src/App.js` (amo na ang main) kag clear ko ang contents, para mabilin lang:
```JSX
function App() {
  return (
    <div>
    </div>
  );
}

export default App;
```
Kung makita mo daw weird may HTML syntax sa javascript, amo na ang [JSX](https://reactjs.org/docs/introducing-jsx.html) syntax nga ginagamit sa React, don't worry same lang man na sa basic HTML kag maagyan ta na karon ang usage.


#### Create form for Addinng Task

Ubrahon ko anay ang form para sa adding sa separate file para mas manageable incase may issue hapos pangitaon kaysa bilog nga code ara lang tanan sa isa ka file. Ma create ko file `AddTask.js` dira lang `src` folder.

Tapos unahon ko anay plastar ang UI (mga HTML elements).

<script src="https://gitlab.com/-/snippets/2056552.js"></script>

Next import ko na sa `src/App.js`, sa main file, as a React Element (daw HTML man guwa ya na - `<AddTask />`) para ma render sa browser.
```JSX
/* src/App.js */

import AddTask from './AddTask';

function App() {
  return (
    <div>
      <AddTask />
    </div>
  );
}

export default App;
```

Next naman ang logic kag actions sa form, unahon ko anay set ang value kung mag type sila sa `<input />`:
```JSX
import { useState } from 'react';

function AddTask({ submit }) {
  const [value, setValue] = useState('');

  return (
    <form>
      <input onChange={(event) => setValue(event.target.value)} />
      <button>Add</button>
      <div>
        Value: {value /* FOR TESTING ONLY */}
      </div>
    </form>
  );
}

export default AddTask;
```
[form-1]

Nag gamit da ko sang [`useState`](https://reactjs.org/docs/hooks-state.html) para ma update sa app ang value. Sa React abi may **state** na nga ginatawag, amo na sya ang gina basehan kung kinanlan sang update or mag rerender sang UI. So daw gin register ko sa React ang `value` nga variable para mabantayan ya kung may changes kag i-save ya.

For example indi ko pag gamitan `useState`, normal variable lang:
```JSX
function AddTask({ submit }) {
  let value = '';

  return (
    <form>
      <input onChange={(event) => value = event.target.value} />
      <button>Add</button>
      <div>
        Value: {value /* WALA NI GA UPDATE ANG `value` */}
      </div>
    </form>
  )
}

export default AddTask;
```
Te gin try mo? nag update? Indi ka gid mag pati mo! hahah

[form-2]



