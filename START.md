# Essential JS quirks and methods to learn before diving into React

### Arrow Function, Spread Operator and Object/Array Destructuring assignment

Ang mga i-discuss ko dri mga features ni sya since **ES6** (ECMAScript 6), daw amo ni ang mga new preferred way mag code subong, mas hapos kag nubo lang. Kag almost throughout sa JS community amo na ni gamit so dapat ka keep up man ta.

Kag suggest ko man try nyu apply kag practice coding with style guide, ang ginagamit ko subong ang [Airbnb JS Style Guide](https://github.com/airbnb/javascript). Nami gid especially kung team kamo, maintidahan nyu code sang isa-kag-isa, kag ikaw man kung balikan mo code mo maindihan mo gid lol.

> [Programs are meant to be read by humans and only incidentally for computers to execute.](https://www.smashingmagazine.com/2012/10/why-coding-style-matters/)


#### Arrow Function

Daw shortened way lang ni mag sulat sang `function` sa javascript -- `() => {}` (*empty function*). 
[https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions).

Before:
```javascript
var sayHello = function(name) {
	return 'Hello, ' + name + '!';
};

console.log(sayHello('John'));
// expect output: Hello, John!

```
With Arrow Function:
```javascript
const sayHello = (name) => `Hello, ${name}!`;
```
So ang amo ni nga expression pwede mo ma sulat to single-line lang. Kung makita mo wala na `return`, after sang `=>` amo na i-return ya.

Kung makita mo instead nga **single quote** string nag gamit ko **backtick/backquote**, amo na sya ang [Template string](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals), mas readable kag concise sya instead sang string concatenation with `+`. Ara man na sa style guide nga gin mention ko sa babaw.

#### Spread operator

Ang spread syntax (`...`) ginagamit na sya mag expand or join sang `Array` or `object`.

Before:
```javascript
/* Combine two arrays */
var arr1 = ['a', 'b', 1];
var arr2 = ['c', 2, 3];
var newArr = arr1.concat(arr2);

console.log(newArr);
// expected output: ['a', 'b', 1, 'c', 2, 3]

/* Combine two objects */
var obj1 = { id: 1, name: 'Glenn' };
var obj2 = { age: 30 };
var newObj = obj1;
for (var key in obj2) {
	newObj[key] = obj2[key];
}

console.log(newObj);
// expected output: {id: 1, name: "Glenn", age: 30}
```

With spread syntax:
```javascript
const arr1 = ['a', 'b', 1];
const arr2 = ['c', 2, 3];
const newArr = [...arr1, ...arr2];

const obj1 = { id: 1, name: 'Glenn' };
const obj2 = { age: 30 };
const newObj = {...obj1, ...obj2};
```
Easy!!!

Ang `object` nga spread pa gid note kung ang duwa ka object with same nga property key, mag combine ang sa punta na nga **value** sundon ya:
```javascript
const obj1 = { id: 1, name: 'Glenn' };
const obj2 = { age: 30, id: 2, address: 'Ledesco' }; // may `id` man nga key
const newObj = {...obj1, ...obj2};

console.log(newObj);
//expected output: {id: 2, name: "Glenn", age: 30, address: "Ledesco"}
````

#### Array and Object Destructuring

Ang [destructuring assignment](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment) pwede mo ni gamiton kung may kwaon ka nga value sa Array or Object.

Before:
```javascript
const arr1 = ['a', 'b', 1];
const value1 = arr1[0];
const value2 = arr1[1];

console.log(value1);
// expected output: a
console.log(value2);
// expected output: b
```
With destructuring:
```javascript
const arr1 = ['a', 'b', 1];
const [value1, value2] = arr1;

console.log(value1);
// expected output: a
console.log(value2);
// expected output: b
```


Sa objects naman ya mag destructuring gina extract mo ang value gamit ang key.

Before:
```javascript
var obj = { id: 1, name: 'Glenn', info: { job: 'Developer' } };
var name = obj.name;
var job = obj.info.job; 
```
With destructuring:
```javascript
const obj = {
  id: 1,
  name: 'Glenn',
  info: {
    job: 'Developer'
  },
};
const { name, info: { job } } = obj; // define `name` and `job` variables

console.log(name);
// expected output: Glenn
console.log(job);
// expected output: Developer
```
Pero kung wala ang key sa object ang value is `undefined`:
```javascript
const obj = { id: 1, name: 'Glenn', info: { job: 'Developer' } };
const { address } = obj;

console.log(address);
// expected output: undefined
```
In function paramaters:
```javascript
const data = { id: 1, name: 'Glenn', info: { job: 'Developer' } };
const getInfo = ({ name, info: { job }}) => `My name is ${name}, I'm a ${job}`;

console.log(getInfo(data));
```
Or kung may kwaon kamo nga key tapos the rest i-save sa another variable:
```javascript
const data = { id: 1, name: 'Glenn', info: { job: 'Developer' } };
const { info, ...rest } = data; // spread syntax

console.log(info);
// expected output: { job: 'Developer' }
console.log(rest);
// expected output: { id: 1, name: 'Glenn' }
```
Kita mo pwede ang spread syntax mag destructure kay kung i-restructure mo ang gamit ang `info` kag `rest` same lang guwaan ya:
```javascript
const data = { id: 1, name: 'Glenn', info: { job: 'Developer' } };
const { info, ...rest } = data;

/* resturcture */
const newData = { info, ...rest };
console.log(newData);
// expected output: { id: 1, name: 'Glenn', info: { job: 'Developer' } }
```

### Array Methods

Daw damo man ni example sa internet kamo na lng check, pero ang mga common nga array methods nyu magamit sa React ang:
* `map()`
* `filter()`
* `find()`
* `reduce()`
* `sort()`
* `push()`

Ang [React](https://reactjs.org/) daan **user interface** kag interaction gina handle ya, indi man tanan nga data halin sa server as-is na, usually gina manipulate mo gid ang data para mag sakto sa interface na gusto mo.
